#!/bin/bash

# This script should be added to the crontab and will send data about available patches for the server
# including a specific entry regarding security patches.

PATCHES=$(sed '2q;d' /var/lib/update-notifier/updates-available | grep -o '[0-9]*')
SECURITY_PATCHES=$(sed "3q;d" /var/lib/update-notifier/updates-available | grep -o '[0-9]*')
INSTANCE_ID=$(wget -q -O - http://169.254.169.254/latest/meta-data/instance-id)
UNIT="Count"
NAMESPACE="System/Linux"

aws cloudwatch put-metric-data --namespace $NAMESPACE --dimensions InstanceId=$INSTANCE_ID --unit $UNIT --metric-name "OperatingSystemUpdates" --value $PATCHES
aws cloudwatch put-metric-data --namespace $NAMESPACE --dimensions InstanceId=$INSTANCE_ID --unit $UNIT --metric-name "OperatingSystemSecurityUpdates" --value $SECURITY_PATCHES
