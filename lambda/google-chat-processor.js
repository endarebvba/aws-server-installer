var https = require('https');
var util = require('util');

exports.handler = function(event, context) {
    console.log(JSON.stringify(event, null, 2));
    console.log('From SNS:', event.Records[0].Sns.Message);

    const message = isJSON(event.Records[0].Sns.Message)
      ? JSON.parse(event.Records[0].Sns.Message)
      : event.Records[0].Sns.Message;
    const environment = process.env.ENVIRONMENT || 'production';

    const postData = createGoogleChatMessage(message);
    const options = {
        method: 'POST',
        hostname: 'chat.googleapis.com',
        port: 443,
        path: environment.toLowerCase().includes('prod')
          ? process.env.CHAT_PRODUCTION_API_PATH
          : process.env.CHAT_TEST_API_PATH
    };

    const req = https.request(options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
        context.done(null);
      });
    });

    req.on('error', function(e) {
      console.log('problem with request: ' + e.message);
    });

    req.write(util.format("%j", postData));
    req.end();
};

function isAWSCloudWatchAlarm(message) {
  return message.hasOwnProperty('AlarmArn');
}

function createGoogleChatMessage(message) {
  if (!isAWSCloudWatchAlarm(message)) {
    return { text: message };
  }

  const content = message.AlarmDescription || message.AlarmName;
  const statusText = getStatusTextFromAlarmState(message);

  return {
    text: `*${statusText}: ${content}*`
  };
}

function getStatusTextFromAlarmState(message) {
  const state = message.NewStateValue.toLowerCase();

  switch (state) {
    case 'alarm':
      return '⚠️ Warning server is at risk';
    case 'ok':
      return '🎉 The following problem has been resolved';
    case 'insufficient_data':
      return `Please double check the AWS alarm with identifier ${message.AlarmName}`;
    default:
      return 'Unsupported state, please contact an AWS administrator';
  }
}

function isJSON(payload) {
  try {
    return !!JSON.parse(payload);
  } catch (err) {
    return false;
  }
}
