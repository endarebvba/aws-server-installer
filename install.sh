#!/bin/sh

set -e

# Globals
RED="\033[0;31m"
GREEN="\033[0;32m"
BLUE="\033[0;34m"
BLANK="\033[0m"

AWS_CW_OFFICIAL_CRON="*/5 * * * * ~/aws-scripts-mon/mon-put-instance-data.pl --mem-used-incl-cache-buff --mem-util --disk-space-util --disk-path=/ --from-cron"
AWS_CW_CUSTOM_CRON="*/5 * * * * ~/aws-scripts-mon/ubuntu-patches-metrics.sh"

AWS_CLOUD_WATCH_MONITORING_SCRIPTS_VERSION="1.2.2"
AWS_CLOUD_WATCH_MONITORING_SCRIPTS_URL="https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-${AWS_CLOUD_WATCH_MONITORING_SCRIPTS_VERSION}.zip"

DOCKER_COMPOSE_DEPLOYER_TEMPLATE_URL="https://bitbucket.org/endarebvba/aws-server-installer/raw/9a1d88cd60aeed12e1ea97fe4ee5428c0b69676f/template/docker-compose.deployer.yml"
DOCKER_COMPOSE_TEMPLATE_URL="https://bitbucket.org/endarebvba/aws-server-installer/raw/9a1d88cd60aeed12e1ea97fe4ee5428c0b69676f/template/docker-compose.yml"
REGISTRY_CREDENTIALS_TEMPLATE_URL="https://bitbucket.org/endarebvba/aws-server-installer/raw/9a1d88cd60aeed12e1ea97fe4ee5428c0b69676f/template/registry_credentials.json"
CUSTOM_METRICS_SCRIPT_URL="https://bitbucket.org/endarebvba/aws-server-installer/raw/9a1d88cd60aeed12e1ea97fe4ee5428c0b69676f/cloudwatch/ubuntu-patches-metrics.sh"

# Parse parameters
for ARGUMENT in "$@"
do

    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)

    case "$KEY" in
            --help)       HELP_REQUIRED=1 ;;
            *)
    esac

done

# Verify parameters
if [ $HELP_REQUIRED ]; then
    echo "${BLUE}"
    echo "This script will set-up the default tooling required on an EC2 instance managed by Endare"
    echo ""
    echo "Parameters"
    echo ""
    echo "\t--help: Prints the information you are currently reading."
    echo "${BLANK}"
    exit 0
fi

# Initialisation
echo "${BLUE}Starting installation${BLANK}"
cd /home/ubuntu
sudo apt-get update

# Install docker
echo "${BLUE}Installing docker${BLANK}"
if ! hash docker 2>/dev/null; then
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable edge"
    apt-cache policy docker-ce
    sudo apt-get install -y docker-ce
    systemctl status docker
    sudo usermod -aG docker ubuntu
else
    echo "${GREEN}Docker already installed${BLANK}"
fi

# Install docker-compose
echo "${BLUE}Installing docker-compose${BLANK}"
if ! hash docker-compose 2>/dev/null; then
    sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    docker-compose --version
else
    echo "${GREEN}Docker-compose already installed${BLANK}"
fi

# Install aws-scripts-mon
echo "${BLUE}Installing aws-scripts-mon${BLANK}"
if [ ! -f /home/ubuntu/aws-scripts-mon/mon-get-instance-stats.pl ]; then
    sudo apt-get install -y unzip libwww-perl libdatetime-perl

    curl "$AWS_CLOUD_WATCH_MONITORING_SCRIPTS_URL" -O

    unzip "CloudWatchMonitoringScripts-${AWS_CLOUD_WATCH_MONITORING_SCRIPTS_VERSION}.zip"
    rm "CloudWatchMonitoringScripts-${AWS_CLOUD_WATCH_MONITORING_SCRIPTS_VERSION}.zip"
else
    echo "${GREEN}aws-scripts-mon already installed${BLANK}"
fi

# Download custom metrics script
echo "${BLUE}Installing custom aws metrics script${BLANK}"
if [ ! -f /home/ubuntu/aws-scripts-mon/ubuntu-patches-metrics.sh ]; then
    curl "$CUSTOM_METRICS_SCRIPT_URL" -o /home/ubuntu/aws-scripts-mon/ubuntu-patches-metrics.sh
else
    echo "${GREEN}custom aws metrics script already installed${BLANK}"
fi

# Install aws cli
echo "${BLUE}Installing aws cli${BLANK}"
if ! hash aws 2>/dev/null; then
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    unzip awscliv2.zip
    sudo ./aws/install
    rm awscliv2.zip
    aws --version
else
    echo "${GREEN}custom aws cli already installed${BLANK}"
fi

# Add cronjobs to crontab
echo "${BLUE}Adding cronjobs${BLANK}"
if crontab -l | grep -q "$AWS_CW_OFFICIAL_CRON"; then
    (crontab -l 2>/dev/null; echo $AWS_CW_OFFICIAL_CRON) | crontab -
fi

if crontab -l | grep -q "$AWS_CW_CUSTOM_CRON"; then
    (crontab -l 2>/dev/null; echo $AWS_CW_CUSTOM_CRON) | crontab -
fi

# Set-up basic deployment stuff
echo "${BLUE}Setting up default deployment files${BLANK}"
mkdir -p /home/ubuntu/deployment
mkdir -p /home/ubuntu/token

# Copy template files if they don't exist yet
if [ ! -f /home/ubuntu/registry_credentials.json ]; then
    curl "$DOCKER_COMPOSE_TEMPLATE_URL" -o /home/ubuntu/deployment/docker-compose.yml
fi

if [ ! -f /home/ubuntu/registry_credentials.json ]; then
    curl "$REGISTRY_CREDENTIALS_TEMPLATE_URL" -o /home/ubuntu/registry_credentials.json
fi

if [ ! -f /home/ubuntu/docker-compose.deployer.yml ]; then
    curl "$DOCKER_COMPOSE_DEPLOYER_TEMPLATE_URL" -o /home/ubuntu/docker-compose.deployer.yml
fi

# Print success
echo "${GREEN}Installation successful!"
echo ""
echo "Next steps:"
echo " - Assign IAM role to your EC2 instance. (If not already configured) (see https://endare.atlassian.net/wiki/spaces/TKB/pages/1688567809/AWS+Cloudwatch+alarms#EC2-IAM-Role)"
echo " - Reconnect to the server to refresh your user's Docker rights (to run Docker without sudo)"
echo "${BLANK}"
