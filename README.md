# AWS Server Installer
## About
This repository contains some helper scripts to set-up a new EC2 instance quickly and reliably. The `install.sh` script is the main entry point for installation and should be copied to the server.

The following configuration can be automated via the script:

   * Docker installation
   * Docker compose installation
   * AWS CLI installation
   * Configuration of sending custom metrics to AWS CloudWatch, including
       * RAM usage
       * Disk space utilisation
       * Available patches for the server
   * Creating default files and folders for use with the Codefresh deployer

## Deployment structure
The Endare deployment structure is as follows

	+-- deployment
	|   +-- docker-compose.yml
	|   +-- <other_project_specific_files_and_folders>
	+-- registry_credentials.json
	+-- docker-compose.deployer.yml
	+-- token
	|   +-- token.txt

## Folder structure notes
The `lambda` folder contains a script `google-chat-processor.js` which is hosted on AWS lambda and
handles incoming CloudWatch alarms, processes them and posts them to the uptime channel on Google Chat.

The `template` folder contains dummy files which the `install.sh` script will copy to the server and
puts them in the correct place as template for the deployment structure.

The `cloudwatch` folder contains custom script(s) which are installed on the server as cronjob and sends
metrics to AWS CloudWatch.

## Read more
 * [Setting up CloudWatch alarms](https://endare.atlassian.net/wiki/spaces/TKB/pages/1688567809/AWS+Cloudwatch+alarms)
 * [Forwarding container logs to CloudWatch](https://endare.atlassian.net/wiki/spaces/TKB/pages/1776353288/AWS+CloudWatch+logging)
